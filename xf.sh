# curl -sL https://rpm.nodesource.com/setup_16.x | sudo bash -
# sudo yum install -y nodejs

# Current directory
DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1 && pwd)"

# Check if .env file exists
if [ ! -f "$DIR/.env" ]; then
  echo "Missing .env file"
  exit 1
fi

set -a
source "$DIR/.env"
set +a

# Check of FORUM_PATH exists and is a directory
if [ ! -d "$FORUM_PATH" ]; then
    echo "FORUM_PATH is not a directory"
    exit 1
fi

# Check of FORUM_PATH/data and FORUM_PATH/internal_data directories exist
if [ ! -d "$FORUM_PATH/data" ]; then
    echo "FORUM_PATH/data does not exist"
    exit 1
fi

if [ ! -d "$FORUM_PATH/internal_data" ]; then
    echo "FORUM_PATH/internal_data does not exist"
    exit 1
fi
# data
if [ -L "./uploads/data" ]; then
    rm "./uploads/data"
fi
ln -s $FORUM_PATH/data/ ./uploads/

# internal_data
if [ -L "./uploads/internal_data" ]; then
    rm "./uploads/internal_data"
fi
ln -s $FORUM_PATH/internal_data/ ./uploads/

# js
if [ -L "./uploads/js" ]; then
    rm "./uploads/js"
fi
ln -s $FORUM_PATH/js/ ./uploads/

npm install
echo "Building the script..."
npm run build
echo "Run using dotenv..."
node --enable-source-maps -r dotenv/config ./build/app.js

echo "Add the following to $FORUM_PATH/src/config.php to activate the bucket:"

config=$(
    cat <<EOF
\$s3 = function()
{
   return new \Aws\S3\S3Client([
      'credentials' => [
         'key' => '$CLOUDFLARE_R2_ACCESS_KEY_ID',
         'secret' => '$CLOUDFLARE_R2_SECRET_ACCESS_KEY',
      ],
      'region' => 'auto',
      'version' => 'latest',
      'endpoint' => '$CLOUDFLARE_R2_ENDPOINT',
   ]);
};

\$config['fsAdapters']['data'] = function() use(\$s3)
{
   return new \League\Flysystem\AwsS3v3\AwsS3Adapter(\$s3(), '$CLOUDFLARE_R2_BUCKET_NAME', 'data');
};
\$config['fsAdapters']['internal-data'] = function() use(\$s3)
{
   return new \League\Flysystem\AwsS3v3\AwsS3Adapter(\$s3(), '$CLOUDFLARE_R2_BUCKET_NAME', 'internal_data');
};
\$config['externalDataUrl'] = function(\$externalPath, \$canonical)
{
   return '$CLOUDFLARE_R2_PUBLIC_URL/data/' . \$externalPath;
};
\$config['javaScriptUrl'] = '$CLOUDFLARE_R2_PUBLIC_URL/js';
EOF
)
echo '# Make sure to install XFAws-2.3.0.zip add-on before applying the changes'
echo "ADD THE FOLLOWING LINES TO YOUR config.php FILE"
echo "// --------------"
echo "$config"
echo "// --------------"

