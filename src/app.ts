import {
    CopyObjectCommand,
    CopyObjectRequest,
    HeadObjectCommand,
    ListBucketsCommand,
    ListObjectsV2Command,
    PutObjectCommand,
    PutObjectCommandInput,
    S3Client
} from '@aws-sdk/client-s3'
import fs from 'fs'
import md5 from 'md5'
import {off} from 'process';

import mime from 'mime-types';

import {
    cloudflareAccountId,
    cloudflareR2AccessKeyId,
    cloudflareR2SecretAccessKey,
    cloudflareR2BucketName,
    maxProcessCount,
    lastModifiedDate,
    skipProcessedDirectories,
    skipExisting
} from './config.js'

console.log('Uploading files from the upload dir...');

const S3 = new S3Client({
    region: 'auto',
    endpoint: `https://${cloudflareAccountId}.r2.cloudflarestorage.com`,
    credentials: {
        accessKeyId: cloudflareR2AccessKeyId,
        secretAccessKey: cloudflareR2SecretAccessKey,
    },
});

const ROOT_DIR = 'uploads';

console.log('Starting upload process...');

let file_process_count = 0;
let directory_process_count = 0;
let errors: {file: string; fileName: string; err: any}[] = [];

const can_process_file = async () => {
    return new Promise((resolve, reject) => {
        const interval = setInterval(() => {
            if (file_process_count < maxProcessCount) {
                clearInterval(interval);
                resolve(true);
            }
        }, 300);
    });
};

const can_process_directory = async () => {
    return new Promise((resolve, reject) => {
        const interval = setInterval(() => {
            if (directory_process_count < maxProcessCount) {
                clearInterval(interval);
                resolve(true);
            }
        }, 300);
    });
};

const awaitNoProcesses = async () => {
    return new Promise((resolve, reject) => {
        const interval = setInterval(() => {
            if (file_process_count === 0) {
                clearInterval(interval);
                resolve(true);
            } else {
                console.log(`Waiting for processes to finish... (process: ${file_process_count})`);
            }
        }, 300);
    });
}

const processUpload = async (file: string) => {
    const fileStream = fs.readFileSync(file);
    const fileName = file.replace(/uploads\//g, '');

    if (
        fileName.includes('.gitkeep')
        || fileName.includes('.is_processed')
    ) {
        return false;
    }

    await can_process_file();
    file_process_count++;

    if (skipExisting) {
        try {
            const checkParams = new HeadObjectCommand({
                Key: fileName,
                Bucket: cloudflareR2BucketName,
            });
            const exists = await S3.send(checkParams);
            if (exists?.$metadata?.httpStatusCode === 200) {
                file_process_count--;
                console.log(`File exists - ${fileName} (process: ${file_process_count})`);
                return true;
            }
        } catch (err) {
            // No need to handle the errors here, most likely just the file does not exist
        }
    }

    const uploadParams: PutObjectCommandInput = {
        Bucket: cloudflareR2BucketName,
        Key: fileName,
        Body: fileStream,
        ContentLength: fs.statSync(file).size,
        ContentType: mime.lookup(file) || 'application/octet-stream',
    };

    const cmd = new PutObjectCommand(uploadParams);

    const digest = md5(fileStream);

    cmd.middlewareStack.add((next) => async (args: any) => {
        args.request.headers['if-none-match'] = `"${digest}"`;
        return await next(args);
    }, {
        step: 'build',
        name: 'addETag'
    })
    // console.log(`Uploading ${fileName}...)`);
    const data = await S3.send(cmd);
    console.log(`Success - ${fileName} - Status Code: ${data.$metadata.httpStatusCode} (process: ${file_process_count})`);
    file_process_count--;

    return true;
}

const correntFileContentType = async (file: string) => {
    const fileStream = fs.readFileSync(file);
    const fileName = file.replace(/uploads\//g, '');
    await can_process_file();
    file_process_count++;

    const copyParams: CopyObjectRequest = {
        Bucket: cloudflareR2BucketName,
        CopySource: `${cloudflareR2BucketName}/${fileName}`,
        Key: fileName,
        ContentType: mime.lookup(file) || 'application/octet-stream',
        MetadataDirective: 'REPLACE',
    };

    const cmd = new CopyObjectCommand(copyParams);
    const data = await S3.send(cmd);
    console.log(`Content type updated - ${fileName}::${copyParams.ContentType} - Status Code: ${data.$metadata.httpStatusCode} (process: ${file_process_count})`);
    file_process_count--;

    return true;
}

const processUploadWithErrors = async (file: string) => {
    const fileName = file.replace(/uploads\//g, '');

    // skip files finishing with.log
    if (fileName.endsWith('.log') || fileName.endsWith('.php')) {
        console.log(`Skipping ${fileName}... - log and php files are not uploaded`);
        return false;
    }

    const stats = fs.statSync(file);

    if (lastModifiedDate > 0) {

        // Skip if file's modification date locally is older than the last modified date
        if (stats.mtimeMs < lastModifiedDate) {
            console.log(`Skipped - ${fileName} - Last Modified Date: ${stats.mtime}`);
            return;
        }
    }

    

    // Store a local file with hash of the file name and its modification date
    const fileHashName = md5(fileName + stats.mtimeMs);
    const fileHashDirectory = `upload_log/${fileHashName[0]}/${fileHashName[1]}/${fileHashName[2]}/${fileHashName[3]}`;
    const logName = `${fileHashDirectory}/` + md5(fileName + stats.mtimeMs) + '.json';

    const mimeType = mime.lookup(file) || 'application/octet-stream';
    if (fs.existsSync(logName)) {
        if (mimeType !== 'application/octet-stream') {
            // We migght need to correct the content type
            // Read the content of the log file as json
            const logFile = fs.readFileSync(logName, 'utf8');
            const logFileJson = JSON.parse(logFile);
            if (!logFileJson.mimeFixed) {
                // File was uploaded incorrectly with wrong mime type
                // correct it
                await correntFileContentType(file);
                logFileJson.mimeFixed = true;
                // write it back to file
                fs.writeFileSync(logName, JSON.stringify(logFileJson));
            }
        }

        console.log(`Skipped - ${fileName} - already uploaded`);
        return;
    }

    const logContent = JSON.stringify({
        fileName: fileName,
        file: file,
        mimeFixed: true,
        stats
    });

    // Make sure file hash directory exists
    if (!fs.existsSync(fileHashDirectory)) {
        fs.mkdirSync(fileHashDirectory, {recursive: true});
    }
    try {
        const uploaded = await processUpload(file);
        if(uploaded) {
            // Write the log file, create if it does not exist
            fs.writeFileSync(logName, logContent, {flag: 'w'});
        }
    } catch (err) {
        file_process_count--;
        if (err?.$metadata?.httpStatusCode === 412) {
            console.log(`Skipped - ${fileName} - Status Code: ${err.$metadata.httpStatusCode} (process: ${file_process_count})`);
            // File exists on the server, write the log file
            fs.writeFileSync(logName, logContent, {flag: 'w'});
            return;
        }
        let error = '';
        if (err.hasOwnProperty('$metadata')) {
            console.error(err.$metadata);
            error = `Error - Status Code: ${err.$metadata.httpStatusCode} - ${err.message} - ${fileName} (process: ${file_process_count})`;
        } else {
            error = `Error - ${fileName} (process: ${file_process_count})`;
        }
        errors.push({
            file: file,
            fileName: process.env.CLOUDFLARE_R2_PUBLIC_URL + '/' + fileName,
            err,
        });
    }
}

const processUploads = async (dirName: string) => {
    console.log('Processing directory ' + dirName);

    // await can_process_directory();

    directory_process_count++;
    const items = fs.readdirSync(dirName, {withFileTypes: true});

    const files = [];
    for (const item of items) {
        if (!item.isDirectory() && !item.isSymbolicLink()) {
            files.push(`${dirName}/${item.name}`);
        }
    }
    const isProcessedFile = dirName + '/.is_processed';

    if (files.length && (!skipProcessedDirectories || !fs.existsSync(isProcessedFile))) {
        console.log(`Found ${files.length} files in ${dirName}`);
        await Promise.all(files.map(processUploadWithErrors));
        // mark the directory as processed by putting a .is_processed file in it
        fs.writeFileSync(isProcessedFile, '1');
    }

    for (const item of items) {
        if (item.isDirectory() || item.isSymbolicLink()) {
            await processUploads(`${dirName}/${item.name}`);
        }
    }

    console.log('Finished processing directory ' + dirName);
    directory_process_count--;
};

processUploads(ROOT_DIR).then(() => {
    console.log('All uploads complete.');
    if (errors.length) {
        console.log('The following files failed to upload. Trying one more time...');
        [...errors].map(async (err) => {
            console.log(err.fileName);
            await processUploadWithErrors(err.file);
        });
    }
});