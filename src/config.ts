const {
    CLOUDFLARE_ACCOUNT_ID,
    CLOUDFLARE_R2_ACCESS_KEY_ID,
    CLOUDFLARE_R2_SECRET_ACCESS_KEY,
    CLOUDFLARE_R2_BUCKET_NAME,
    MAX_PROCESS_COUNT,
    LAST_MODIFIED_DATE,
    SKIP_PROCESSED_DIRECTORIES,
    SKIP_EXISTING
} = process.env

if (
    !CLOUDFLARE_ACCOUNT_ID ||
    !CLOUDFLARE_R2_ACCESS_KEY_ID ||
    !CLOUDFLARE_R2_SECRET_ACCESS_KEY ||
    !CLOUDFLARE_R2_BUCKET_NAME
) {
    throw new Error('Missing environment variables.')
}

const cloudflareAccountId: string = CLOUDFLARE_ACCOUNT_ID
const cloudflareR2AccessKeyId: string = CLOUDFLARE_R2_ACCESS_KEY_ID
const cloudflareR2SecretAccessKey: string = CLOUDFLARE_R2_SECRET_ACCESS_KEY
const cloudflareR2BucketName: string = CLOUDFLARE_R2_BUCKET_NAME
const maxProcessCount: number = parseInt(MAX_PROCESS_COUNT || '100')
const lastModifiedDate: number = parseInt(LAST_MODIFIED_DATE || '0')
const skipProcessedDirectories = SKIP_PROCESSED_DIRECTORIES === 'true' ? true: false;
const skipExisting = SKIP_EXISTING === 'true' ? true: false;

export {
    cloudflareAccountId,
    cloudflareR2AccessKeyId,
    cloudflareR2SecretAccessKey,
    cloudflareR2BucketName,
    maxProcessCount,
    lastModifiedDate,
    skipProcessedDirectories,
    skipExisting
}
